package main

import (
	"encoding/json"
	"fmt"
	"github.com/jftuga/geodist"
	"io"
	"log/slog"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// Aircraft is used for unmarshalling, see https://github.com/wiedehopf/readsb/blob/dev/README-json.md
type Aircraft struct {
	Hex        string     `json:"hex"`
	MarkerType string     `json:"type"`
	CallSign   string     `json:"flight"`
	Latitude   float64    `json:"lat"`
	Longitude  float64    `json:"lon"`
	Altitude   json.Token `json:"alt_baro"`
}

type ADSBData struct {
	Time     float64    `json:"now"`
	Messages uint64     `json:"messages"`
	Planes   []Aircraft `json:"aircraft"`
}

type SeenFlight struct {
	FirstSeen   time.Time
	LastSeen    time.Time
	CallSign    string
	MinDistance float64
	Lowest      float64
}

var seenFlights map[string]SeenFlight

func main() {

	myHost := os.Getenv("ADSBFEED_HOST")

	url := "http://" + myHost + "/adsbx/data/aircraft.json"

	minAltitude := float64(50)
	maxAltitude := float64(50000)

	myLat := os.Getenv("ADSBFEED_LAT")
	myLatFloat, err := strconv.ParseFloat(myLat, 64)
	if err != nil {
		fmt.Println("failed parsing lat")
		return
	}

	myLon := os.Getenv("ADSBFEED_LON")
	myLonFloat, err := strconv.ParseFloat(myLon, 64)
	if err != nil {
		fmt.Println("failed parsing lat")
		return
	}

	myLoc := geodist.Coord{Lat: myLatFloat, Lon: myLonFloat}
	maxDistance := float64(20)
	pollInterval := time.Second * 5
	disappearTime := time.Minute
	seenFlights = make(map[string]SeenFlight)

	for {
		PollAndProcess(url, minAltitude, maxAltitude, myLoc, maxDistance, disappearTime)
		time.Sleep(pollInterval)
	}
}

func PollAndProcess(url string, minAltitude float64, maxAltitude float64, myLoc geodist.Coord, maxDistance float64, disappearTime time.Duration) {
	req, _ := http.NewRequest("GET", url, nil)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)

	var myADSBData ADSBData

	err := json.Unmarshal(body, &myADSBData)
	if err != nil {
		slog.Error("failed unmarshalling", "err", err)
		return
	}

	//fmt.Printf("know about %d flights\n", len(seenFlights))
	for _, flight := range myADSBData.Planes {
		planeLoc := geodist.Coord{Lat: flight.Latitude, Lon: flight.Longitude}
		distanceMiles, _ := geodist.HaversineDistance(myLoc, planeLoc)
		altitudeNum, valid := flight.Altitude.(float64)
		if !valid {
			switch flight.Altitude.(type) {
			case nil:
				altitudeNum = 0
			case string:
				if flight.Altitude == "ground" {
					altitudeNum = 0
				}
			default:
				fmt.Printf("weird altitude: %v %T\n", flight.Altitude, flight.Altitude)
				continue
			}
		}

		if altitudeNum < minAltitude || altitudeNum > maxAltitude || distanceMiles > maxDistance {
			continue
		}

		flight.CallSign = strings.Trim(flight.CallSign, " ")

		_, valid = seenFlights[flight.Hex]
		var newSeen time.Time
		var newClosest float64
		var newLowest float64

		if !valid {
			fmt.Printf("found new flight: date: %v callsign: %s hex: %s lat: %f, lon: %f altitude: %.0f distance: %.2f miles\n", time.Now().Format(time.RFC3339), flight.CallSign, flight.Hex, flight.Latitude, flight.Longitude, flight.Altitude, distanceMiles)
			newSeen = time.Now()
			newClosest = distanceMiles
			newLowest = altitudeNum
		} else {
			newSeen = seenFlights[flight.Hex].FirstSeen
			if distanceMiles < seenFlights[flight.Hex].MinDistance {
				newClosest = distanceMiles
			} else {
				newClosest = seenFlights[flight.Hex].MinDistance
			}
			if altitudeNum < seenFlights[flight.Hex].Lowest {
				newLowest = altitudeNum
			} else {
				newLowest = seenFlights[flight.Hex].Lowest
			}
		}

		seenFlights[flight.Hex] = SeenFlight{
			FirstSeen:   newSeen,
			LastSeen:    time.Now(),
			CallSign:    flight.CallSign,
			MinDistance: newClosest,
			Lowest:      newLowest,
		}
	}

	keys := make([]string, len(seenFlights))

	i := 0
	for k := range seenFlights {
		keys[i] = k
		i++
	}

	for _, key := range keys {
		lastSeenTime := seenFlights[key].LastSeen
		byeTime := time.Now().Sub(lastSeenTime)
		if byeTime > disappearTime {
			visibleTime := seenFlights[key].LastSeen.Sub(seenFlights[key].FirstSeen)
			fmt.Printf("flight disappeared: date: %v callsign: %s hex: %s url: https://globe.adsbexchange.com/?icao=%s was visible for %s, closest measurement was %0.2f miles\n", time.Now().Format(time.RFC3339), seenFlights[key].CallSign, key, key, visibleTime.Round(time.Second), seenFlights[key].MinDistance)
			if seenFlights[key].MinDistance < 5 {
				if seenFlights[key].MinDistance < 1 && seenFlights[key].Lowest < 5000 {
					fmt.Printf("That one shook the house\n")
				} else {
					fmt.Printf("That was a close one!\n")
				}
			}
			delete(seenFlights, key)
		}
	}
}
